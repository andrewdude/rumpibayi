$('#navCollapseButton').click(function(){
	$(this).toggleClass('collapsed');
	$('#navCollapse').slideToggle();
});

$('.header-slider').slick({
	infinite: true,
	slidesToShow: 1,
	slidesToScroll: 1,
	speed: 500,
	fade: true,
	cssEase: 'linear'
});

$('.carousel-items').slick({
	infinite: true,
	slidesToShow: 3,
	slidesToScroll: 1,
	speed: 350,
	responsive: [
		{
			breakpoint: 991,
			settings: {
				slidesToShow: 2
			}
		},
		{
			breakpoint: 767,
			settings: {
				slidesToShow: 1
			}
		}
	]
});